import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TraderService {

  constructor(private http:HttpClient) { }

  serviceURL = 'http://citieurlinux7.conygre.com:8080/api/stock'


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError('something bad happened');
    };
  }

  getData() {
    return this.http.get(`${this.serviceURL}`)
    .pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  getDataParams(category){
    return this.http.get(`${this.serviceURL}${category}`)
    .pipe(this.handleError<Object[]>('getDataParams', []));
    
  }
}
