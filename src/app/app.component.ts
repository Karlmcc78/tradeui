import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TraderService } from './trader.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'trader-ui';
 trades:Object[] = [{cat:'AAPL'}, {cat: 'FB'}, {cat: 'AMZN'}, {cat:'TSLA'}, {cat:'MSFT'}, {cat:'NKE'},{cat:'TMUS'},{cat:'VOD.L'}]
 whichCategory:string | boolean
 model
 showFilter = false
 showFilterToday = false
 showLatest = false
 serviceURL="http://citieurlinux7.conygre.com:8080/api/stock"

  ngOnInit(){
    this.showFilterToday=false
    this.showFilter = false
    this.invokeService();
  }

  constructor(private traderService:TraderService,private http:HttpClient){}

  handleClick(){
    this.showFilter=false
    this.showFilterToday=false
    this.showLatest = false
    this.invokeService();
   
  }
  // handleClickSearch(){
  // this.traderService.getDataParams(this.whichCategory) .subscribe((result)=>{this.model=result})
  // }

  dateStr1
  fullDate
  handleTodayClick(){
    this.showFilterToday = true
    this.showFilter=false
    this.showLatest = true

    this.dateStr1 = new Date().toISOString().substr(0, 10);
    this.fullDate = this.dateStr1+'T00:00:00'
    console.log(this.fullDate)
  }

  handleChange(){
    this.showFilter=true
    this.showFilterToday=false
    this.showLatest = true
    

  } 

  onSubmit(data){
    this.http.post('http://citieurlinux7.conygre.com:8080/api/stock',data)
    .subscribe((result)=>{
      console.log("result: ", result)
    })
   console.log(data)
  }

  

  allTrades;
  invokeService(){
    this.traderService.getData().subscribe( (result)=>{
      this.allTrades = result
      console.log(result)} );
  }

}
